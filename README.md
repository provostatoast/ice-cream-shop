## Tasty's Ice Cream Shoppe

Thank you for helping us bring ice cream to the internet!

This is a basic `create-react-app` bootstrapped with redux, typescript, and tailwindcss.

Please `npm i` and `npm start` to get started.
