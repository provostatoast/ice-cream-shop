import React from "react";
import { HelloWorld } from "./components/HelloWorld";

function App() {
  return (
    <div className="h-screen flex flex-col justify-center items-center">
      <HelloWorld />
    </div>
  );
}

export default App;
