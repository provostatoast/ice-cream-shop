import React, { useState } from "react";
import { Wave } from "./icons/Wave";
import { Ticket } from "./icons/Ticket";
import NowScooping from "./NowScooping";

export const HelloWorld = () => {
  const [number, setNumber] = useState(1);
  const increment = () => {
    let val = number + 1;
    setNumber(val);
  };
  return (
    <div className="flex flex-col justify-center relative">
      <div id="header">Tasty's Ice Cream Shoppe</div>
      <div className="ticket" onClick={increment}>
        <div className="number">{number}</div>
        <Ticket />
      </div>
      <span className="mt-6 text-center">take a number</span>
      <NowScooping number={number} />
    </div>
  );
};
