import React, { useState, useEffect } from "react";
import { Scoop } from "../icons/Scoop";

const iceCreams = [
  { name: "chocolate", color: "brown" },
  { name: "strawberry", color: "pink" },
];

const NowScooping = ({ number }) => {
  const [currentVal, setCurrentVal] = useState(1);
  const nowServingNumber = () => {
    setTimeout(() => {
      if (currentVal < 9) {
        setCurrentVal(currentVal + 1);
      } else {
        setCurrentVal(1);
      }
    }, 2000);
  };

  useEffect(() => {
    nowServingNumber();
  });

  return (
    <>
      <div id="placeInLine">
        <div>Now Scooping</div> <div>{number}</div>
        <div>{currentVal}</div>
      </div>
      {iceCreams.map((ic) => {
        return (
          <>
            <Scoop color={ic.color} />
            {ic.name}
          </>
        );
      })}
    </>
  );
};

export default NowScooping;

// address types below...
