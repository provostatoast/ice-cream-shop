import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../store/store";
import { HelloWorld } from "../HelloWorld";

test("renders hello world", () => {
  const { getByText } = render(
    <Provider store={store}>
      <HelloWorld />
    </Provider>
  );

  expect(getByText(/hello world/i)).toBeInTheDocument();
});
