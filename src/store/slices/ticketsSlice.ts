import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

interface TicketsState {
  tickets: number[];
}

const initialState: TicketsState = {
  tickets: [],
};

const ticketsSlice = createSlice({
  name: "tickets",
  initialState,
  reducers: {
    setTickets: (state, action: PayloadAction<number[]>) => {
      state.tickets = action.payload;
    },
  },
});

export const { setTickets } = ticketsSlice.actions;

export const selectTickets = (state: RootState) => state.tickets.tickets;

export default ticketsSlice.reducer;
