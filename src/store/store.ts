import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import ticketsReducer from "./slices/ticketsSlice";

export const store = configureStore({
  reducer: {
    tickets: ticketsReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
